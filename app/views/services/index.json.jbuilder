json.array!(@services) do |service|
  json.extract! service, :id, :name, :remote_id, :start_date, :raw_html_1, :raw_html_2, :type, :slug
  json.url service_url(service, format: :json)
end
