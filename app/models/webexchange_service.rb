class WebexchangeService < Service
  require 'WebexchangeAdapter.rb'
  require 'WebexchangeParser.rb'
  
  # fetches the entry from remote
  # optional parameters:
  # webex_adapter, pass in an adapter to avoid repeated authentication
  def fetch( webex_adapter=nil )
    f = webex_adapter || WebexchangeAdapter.new( ENV["EXTRANET_USERNAME"], ENV["EXTRANET_PASSWORD"] )
    
	  this_month = f.fetch_schedule(self.remote_id, Date.today.beginning_of_month)
	  next_month = f.fetch_schedule(self.remote_id, Date.today.next_month.beginning_of_month)
	  self.raw_html_1 = this_month
	  self.raw_html_2 = next_month
	  touch
	  save
  end
  
  def parse
    WebexchangeParser.new(raw_html_1).as_json + WebexchangeParser.new(raw_html_2).as_json
    # [{message: "see webexchange_service.rb, parse method"}]
  end
end