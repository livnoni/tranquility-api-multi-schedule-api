class ServicesController < ApplicationController
  before_action :set_service, only: [:show, :edit, :update, :destroy, :fetch, :next]
  before_action :restrict_access_to_admins_unless_none_exist, only: [:new, :edit, :update, :create, :destroy]
  before_action :restrict_access_to_users_unless_none_exist, only: [:show]
  
  include ActionView::Helpers::DateHelper # to use time_ago_in_words

  # renders page two of the raw html
  def next
    respond_to do |format|
      if @service
        format.html { render inline: @service.raw_html_2 || "There are no data for #{@service.name} as of #{ time_ago_in_words( @service.updated_at) } ago. Please try to fetch."}
      else
        format.html { render inline: 'not found' }
      end
    end
    
  end
  
  # GET /services
  # GET /services.json
  def index
    @services = Service.all
    
    respond_to do |format|
      format.html
      format.json { render json: @services.collect {|e| e.as_json(only: [:id, :name, :remote_id, :type, :slug])} }
    end
  end

  # GET /services/1
  # GET /services/1.json
  def show
    @specific_date = params[:date] ? Date.parse( params[:date]) : nil
    
    respond_to do |format|
      if @service
        format.html { render inline: @service.raw_html_1 || "There are no data for #{@service.name} as of #{ time_ago_in_words( @service.updated_at) } ago. Please try to fetch."}
        format.json { render json: @specific_date ? @service.parse.select {|e| @specific_date == e[:shift_start].in_time_zone.to_date } : @service.parse }
      else
        format.html { render inline: 'not found' }
        format.json { render json: {error: "not found"}, status: :unprocessable_entity }
      end
    end
  end

  def fetch
    @service.fetch
    redirect_to service_path @service
  end
  
  # GET /services/new
  def new
    @service = Service.new
  end

  # GET /services/1/edit
  def edit
  end

  # POST /services
  # POST /services.json
  def create
    @service = Service.new(service_params)

    respond_to do |format|
      if @service.save
        format.html { redirect_to @service, notice: 'Service was successfully created.' }
        format.json { render action: 'show', status: :created, location: @service }
      else
        format.html { render action: 'new' }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /services/1
  # PATCH/PUT /services/1.json
  def update
    respond_to do |format|
      if @service.update(service_params)
        format.html { redirect_to @service, notice: 'Service was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /services/1
  # DELETE /services/1.json
  def destroy
    @service.destroy
    respond_to do |format|
      format.html { redirect_to services_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service
      @service = Service.find_by_slug(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_params
      params.require(:service).permit(:name, :remote_id, :start_date, :raw_html_1, :raw_html_2, :type, :slug)
    end
end
