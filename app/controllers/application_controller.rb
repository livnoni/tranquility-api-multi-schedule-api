class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  @current_user = nil
  helper_method :current_user, :restrict_access_to_users_unless_none_exist, :restrict_access_to_admins_unless_none_exist
  
  private
  def restrict_access_to_admins_unless_none_exist
    if User.where(admin: true).size == 0
      # allow to continue
    elsif current_user and current_user.admin
      # allow to continue
    else
      render "layouts/restricted"      
    end
  end

  def restrict_access_to_users_unless_none_exist
    if User.all.size == 0
      # allow to continue
    elsif current_user 
      # allow to continue
    else
      render "layouts/restricted"      
    end
  end
  
  def current_user
    cookies[:key] = params[:key] if params[:key]
    @current_user ||= User.where( key: cookies[:key] ).first
  end
  
end
