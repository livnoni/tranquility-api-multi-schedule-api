class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.string :remote_id
      t.date :start_date
      t.text :raw_html_1
      t.text :raw_html_2
      t.string :type
      t.string :slug

      t.timestamps
    end
  end
end
