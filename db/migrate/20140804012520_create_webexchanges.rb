class CreateWebexchanges < ActiveRecord::Migration
  def change
    create_table :webexchanges do |t|
      t.string :name
      t.string :service_code
      t.text :this_month
      t.text :next_month

      t.timestamps
    end
  end
end
