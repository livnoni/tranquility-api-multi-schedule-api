class AddNextMonthToAmion < ActiveRecord::Migration
  def change
    add_column :amions, :on_call_next_month, :text
    add_column :amions, :all_shifts_next_month, :text
    add_column :amions, :people, :text
    remove_column :amions, :users
  end
end
