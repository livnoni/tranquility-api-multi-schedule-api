# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140902161813) do

  create_table "amions", force: true do |t|
    t.string   "account"
    t.text     "rows"
    t.text     "on_call"
    t.text     "all_shifts"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "on_call_next_month"
    t.text     "all_shifts_next_month"
    t.text     "people"
  end

  create_table "services", force: true do |t|
    t.string   "name"
    t.string   "remote_id"
    t.date     "start_date"
    t.text     "raw_html_1"
    t.text     "raw_html_2"
    t.string   "type"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "key"
    t.boolean  "admin",      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "webexchanges", force: true do |t|
    t.string   "name"
    t.string   "service_code"
    t.text     "this_month"
    t.text     "next_month"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
