# Usage
# f = WebexchangeAdapter.new
# f.fetch_schedule
class WebexchangeAdapter
  @penn_extranet_adapter = nil
  
  def initialize(username, password)
    @penn_extranet_adapter = PennExtranetAdapter.new( username, password)
  end
  
  def authenticated_agent
    @penn_extranet_adapter.authenticated_agent
  end

  def fetch_schedule( service_code = "      LC25", date_par=Date.today )
    if service_code.length != 10
  	  puts "** Warning: expecting code to be 10 chars long, not #{service_code.length}" 
      puts "** this could result in empty calendar"
    end
    url = create_url(service_code, date_par)
  	begin
  	  
    	res = authenticated_agent.post(url, 
        "__EVENTTARGET" => "", 
    		"__EVENTARGUMENT" => "",
    		"__VIEWSTATE" => "/wEPDwULLTE4MzUzOTU5MDMPZBYCAgMPZBYOZg8PFgIeBFRleHQFD0VQUyBPZmZpY2UsIEhVUGRkAgQPDxYCHgdUb29sVGlwBQlTaG93IGhlbHAWAh4HT25DbGljawUccmV0dXJuIG9wZW5oZWxwd2luZG93KCcnLCcnKWQCBg8PZBYCHgdvbmNsaWNrBR9yZXR1cm4gY2hlY2tEYXRlKHR4dFN0YXJ0RGF0ZSk7ZAIIDw9kFgIeBXRpdGxlBUZTZWxlY3QgTGFuZHNjYXBlIHBhZ2Ugb3JpZW50YXRpb24NCndoZW4gcHJpbnQgZGlhbG9nIGJveCB3aWxsIGFwcGVhciAhZAILDxAPZBYCHwMFPGphdmFzY3JpcHQ6b2NSZXBvcnRTaG93SGlkZVRpbWUoY21iU3RhcnRUaW1lLCBjbWJTdGFydEZsYWcpO2RkZAIMDxBkEBUYBTEyOjAwBTEyOjMwBTAxOjAwBTAxOjMwBTAyOjAwBTAyOjMwBTAzOjAwBTAzOjMwBTA0OjAwBTA0OjMwBTA1OjAwBTA1OjMwBTA2OjAwBTA2OjMwBTA3OjAwBTA3OjMwBTA4OjAwBTA4OjMwBTA5OjAwBTA5OjMwBTEwOjAwBTEwOjMwBTExOjAwBTExOjMwFRgFMTI6MDAFMTI6MzAFMDE6MDAFMDE6MzAFMDI6MDAFMDI6MzAFMDM6MDAFMDM6MzAFMDQ6MDAFMDQ6MzAFMDU6MDAFMDU6MzAFMDY6MDAFMDY6MzAFMDc6MDAFMDc6MzAFMDg6MDAFMDg6MzAFMDk6MDAFMDk6MzAFMTA6MDAFMTA6MzAFMTE6MDAFMTE6MzAUKwMYZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZGQCEQ8WAh4HVmlzaWJsZWhkGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYFBQZJQkhlbHAFC2Noa1Nob3dUaW1lBQ5jaGtTaG93RXh0SW5mbwUTY2hrU2hvd0luc3RydWN0aW9ucwUOY2hrSGlkZVBhZ2VySURPqpTa4Rm9Q3rF4IRRxgSAf/keTQ==",
    		"__EVENTVALIDATION" => "/wEWKwKfuf6gAwKH//+GAgKB6azWDAKukNaUDAKTgLz1AwLZ4qbEBgKgotf5DQLunbmSCAKRmOT1CgLf76qABwKj74aEDQLymLulDwLg2ZN+AtTx8rkJAtzE4ogPAtzEvpkFAv/E/osPAv/EupgFAv/E4ogPAv/EvpkFAv/E5okPAv/Eop4FAv/E6o4PAv/Epp8FAv/E7o8PAv/EqpwFAv/E0owPAv/Erp0FAv/E1o0PAv/EkpIFAv/Emr0PAv/E1oMFAv/EnrIPAv/E2oAFAtzE+ooPAtzEtpsFAtzE/osPAtzEupgFAvSSrNAHAtuSrNAHAsff0b8KArj//fIMArTZif4PIkHMJWGSXlsJJrqCbx9oY8iGvG0=",
    		"hidOnCallProfile" => service_code,
    		"hidOCPagerID" => "",
    		"rbChose" => "Monthly Report",
    		"butGenerateExcel" => "Create Report",
    		"txtStartDate" => date_par.strftime("%-m/%d/%Y"), #"9/01/2013",
    		"cmbStartTime" => "12:00",
    		"cmbStartFlag" => "AM"
    	)
    	res.body
    rescue
      raise "Failed to connect. Are you on VPN?"
      nil
    end
  end
    
  def onVPN?() #true if on VPN, false if not, I appologize for using exceptions for flow control
    begin
      Mechanize.new.get('http://uphsxnet.uphs.upenn.edu')
      return true
    rescue Mechanize::ResponseCodeError
      return false
    end
  end
    
  def create_url(service_code, date_par)
    base_url = onVPN? ? 'http://uphamcmweb01/webdirectory/OnCallGenReport.aspx' : "https://extranet.uphs.upenn.edu/webdirectory/,DanaInfo=uphamcmweb01+OnCallGenReport.aspx"
    parameters= "?ProfileID=#{service_code}&PagerID=&month=#{date_par.strftime("%-m")}&year=#{date_par.strftime("%Y")}"
    full_url = base_url + parameters
    uri = URI::encode(full_url)
  end
  
end