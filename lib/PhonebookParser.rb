class PhonebookParser
  EMAIL_REGEX = /[\w+\-.]+@[a-z\d\-.]+\.[a-z]+/i
  def parse(page, options = nil) #returns a hash of results
    array_of_raw_text_hashes = pull_result_text_strings(page)
    result = []
    array_of_raw_text_hashes.each do |person|
      result << parse_person_hash(person)
    end
    result
  end
  
  def get_source_code_from_page(page)
    if page.class.to_s == 'String'
       page[400...page.length]
    else
       page.body[400...page.body.length]
    end
  end
  
  def pull_result_text_strings(page)
    source_code = get_source_code_from_page(page) 
    result = []
    text_page_results = [] # each person, when parsed by record, gives 3 lines for result. in addition, scanning by input.icon gives a text string every 3rd time. 
    Nokogiri::HTML(source_code).css('.record').each do |lines|
      result.push(lines.inner_text)
      text_page_results.push lines.css('input.icon').to_s.scan(EMAIL_REGEX)
    end
    num_of_results = result.length/3
    array_of_results = []
    (0...num_of_results).each do |person|
      count = person * 3
      array_of_results.push({:name_string => result[count], :location_string => result[count+1], :contact_info_string => result[count+2], :text_page_string => text_page_results[count+2].first})
    end
    array_of_results
  end
  
  def parse_location_string(location)
    location.scan(/\S*\b/).inject do |memo, word|
      memo = "#{memo} #{word}"
    end
  end
  
  def parse_person_hash(person)
   contact_info = parse_contact_information(person[:contact_info_string])
   result = 
   { 
    :name => parse_name_string(person[:name_string]), 
    :location => parse_location_string(person[:location_string]),
    :text_page_address => parse_text_page_address(person[:text_page_string])
    }
    
    contact_info.each {|k, v| result[k] = v}
    result
  end
  
  def parse_text_page_address(string)
    string
  end
  
  def parse_name_string(string)
     string.strip.match(/\A.*/)[0].rstrip
  end
  
  def parse_contact_information(string)
   numbers = string.scan(/\(\d\d\d\) \d\d\d-\d\d\d\d \(\D*\)/)
   if numbers == nil
     return "no matches found while searching the result string"
   end
   result = {}
   numbers.each do |number|
     key = number.match(/\(\D*\)/)
     key = key[0]
     key = key[1...(key.length-1)]
     
     val = number.match(/\(\d\d\d\) \d\d\d-\d\d\d\d/)[0]
     result[key] = val
   end
   result['email'] = string.match(EMAIL_REGEX)[0] if string =~ EMAIL_REGEX
   result
  end
    
  def search_results_text_string(name_hash)
    pull_result_text_strings search_result_page(name_hash)
  end
  
end